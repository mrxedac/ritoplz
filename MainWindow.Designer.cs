﻿namespace RitoPlz
{
    partial class MainWindow
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("League of Legends process list");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Other process list");
            this.btnExit = new System.Windows.Forms.Button();
            this.treeList = new System.Windows.Forms.TreeView();
            this.label1 = new System.Windows.Forms.Label();
            this.lblProcName = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblCollides = new System.Windows.Forms.Label();
            this.lblCollideInfo = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnReport = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(441, 521);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "/ff";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.button1_Click);
            // 
            // treeList
            // 
            this.treeList.Location = new System.Drawing.Point(11, 235);
            this.treeList.Name = "treeList";
            treeNode1.Name = "nodeRito";
            treeNode1.Text = "League of Legends process list";
            treeNode2.Name = "nodeOther";
            treeNode2.Text = "Other process list";
            this.treeList.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            this.treeList.Size = new System.Drawing.Size(505, 174);
            this.treeList.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 423);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Double click a process for more information.";
            // 
            // lblProcName
            // 
            this.lblProcName.AutoSize = true;
            this.lblProcName.BackColor = System.Drawing.Color.Transparent;
            this.lblProcName.ForeColor = System.Drawing.Color.White;
            this.lblProcName.Location = new System.Drawing.Point(12, 440);
            this.lblProcName.Name = "lblProcName";
            this.lblProcName.Size = new System.Drawing.Size(0, 13);
            this.lblProcName.TabIndex = 3;
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.BackColor = System.Drawing.Color.Transparent;
            this.lblDescription.ForeColor = System.Drawing.Color.White;
            this.lblDescription.Location = new System.Drawing.Point(12, 457);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(0, 13);
            this.lblDescription.TabIndex = 4;
            // 
            // lblCollides
            // 
            this.lblCollides.AutoSize = true;
            this.lblCollides.BackColor = System.Drawing.Color.Transparent;
            this.lblCollides.ForeColor = System.Drawing.Color.White;
            this.lblCollides.Location = new System.Drawing.Point(12, 474);
            this.lblCollides.Name = "lblCollides";
            this.lblCollides.Size = new System.Drawing.Size(0, 13);
            this.lblCollides.TabIndex = 5;
            // 
            // lblCollideInfo
            // 
            this.lblCollideInfo.AutoSize = true;
            this.lblCollideInfo.BackColor = System.Drawing.Color.Transparent;
            this.lblCollideInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCollideInfo.ForeColor = System.Drawing.Color.White;
            this.lblCollideInfo.Location = new System.Drawing.Point(12, 500);
            this.lblCollideInfo.Name = "lblCollideInfo";
            this.lblCollideInfo.Size = new System.Drawing.Size(0, 13);
            this.lblCollideInfo.TabIndex = 6;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::RitoPlz.Properties.Resources.bkdf7garkgab1a0pjwlj;
            this.pictureBox1.Location = new System.Drawing.Point(9, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(204, 196);
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(225, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(292, 30);
            this.label2.TabIndex = 8;
            this.label2.Text = "RitoPlz Network analysis tool";
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(228, 83);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(292, 128);
            this.label3.TabIndex = 9;
            this.label3.Text = "RitoPlz analyses the currently running processes on your computer, and show which" +
    " one of them could collide with League\'s TCP and UDP sockets, and could cause th" +
    "e recent Windows 10 bugsplats.";
            // 
            // btnReport
            // 
            this.btnReport.Location = new System.Drawing.Point(360, 521);
            this.btnReport.Name = "btnReport";
            this.btnReport.Size = new System.Drawing.Size(75, 23);
            this.btnReport.TabIndex = 10;
            this.btnReport.Text = "x9 rito";
            this.btnReport.UseVisualStyleBackColor = true;
            this.btnReport.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(23, 544);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(485, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Full credits to Cheyne Wallace (http://www.cheynewallace.com/) for the awesome po" +
    "rt parsing code.";
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::RitoPlz.Properties.Resources.taric_rito1;
            this.ClientSize = new System.Drawing.Size(528, 559);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnReport);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lblCollideInfo);
            this.Controls.Add(this.lblCollides);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblProcName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.treeList);
            this.Controls.Add(this.btnExit);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.Text = "RitoPlz by MrXedac";
            this.Load += new System.EventHandler(this.MainWindow_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.TreeView treeList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblProcName;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblCollides;
        private System.Windows.Forms.Label lblCollideInfo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnReport;
        private System.Windows.Forms.Label label4;
    }
}

