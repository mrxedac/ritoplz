﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RitoPlz
{
    public partial class MainWindow : Form
    {
        private List<Port> lp;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {
            treeList.DoubleClick += UpdateInfo;

            /* Get nodes */
            TreeNode rito = treeList.Nodes[0];
            TreeNode oth = treeList.Nodes[1];

            /* Find processes */
            lp = Port.GetNetStatPorts();

            /* Parse ! */
            foreach(Port p in lp)
            {
                TreeNode n = new TreeNode(p.name);
                n.Tag = (object)p;
                /* This is crappy, but who cares */
                if (p.process_name.Contains("Lol") || p.process_name.Contains("LoL") || p.process_name.Contains("League") || p.process_name.Contains("Maestro") || p.process_name.Contains("RADS") || p.process_name.Contains("Riot"))
                {
                    rito.Nodes.Add(n);
                }
                else
                {
                    oth.Nodes.Add(n);
                }
                    
            }

            /* Show some info */
            lblCollideInfo.Text = oth.Nodes.Count.ToString() + " processes could cause some networking problems with League.";

            /* Expand nodes */
            rito.Expand();
            oth.Expand();
        }

        public void UpdateInfo(object sender, EventArgs e)
        {
            /* Get Port info hidden in TreeNode */
            TreeView v = (TreeView)sender;
            TreeNode t = v.SelectedNode;
            if (t == null)
                return;
            if (t.Text.Contains("process list"))
                return;
            Port info = (Port)t.Tag;

            /* Fill in process name */
            lblProcName.Text = info.name;

            /* Fill in description */
            switch(info.process_name)
            {
                case "LoLPatcher":
                    lblDescription.Text = "League of Legends Patcher";
                    break;
                case "LolClient":
                    lblDescription.Text = "League of Legends Client";
                    break;
                case "svchost":
                    lblDescription.Text = "Windows NT Service Host";
                    break;
                case "mDNSResponder":
                    lblDescription.Text = "Apple Bonjour";
                    break;
                case "vmware-hostd":
                    lblDescription.Text = "VMWare Host Daemon";
                    break;
                case "TeamViewer_Service":
                    lblDescription.Text = "TeamViewer Network Service";
                    break;
                default:
                    lblDescription.Text = "I don't know what this is.";
                    break;
            }

            /* Fill in collide info */
            int portint = int.Parse(info.port_number);
            if (t.Parent.Text.Contains("Other"))
            {
                if ( /* Maestro */ portint >= 8393 && portint <= 8400)
                    lblCollides.Text = "Could collide with Maestro / League Patcher";

                if ( /* LoL game client */ portint >= 5000 && portint <= 5500)
                    lblCollides.Text = "Could collide with LoL Game Client";
                
                if ( /* PVP.Net */ (portint == 2099 || portint == 5222 || portint == 5223))
                    lblCollides.Text = "Could collide with PvP.Net";

                if ( /* HTTP(S) */ portint == 80 || portint == 443)
                    lblCollides.Text = "Could collide with HTTP connections (are you running a web server ?)";

                if ( /* Spectator mode */ (portint == 8088))
                    lblCollides.Text = "Could collide with LoL Spectator";
            }
            else
            {
                lblCollides.Text = "Legit League process.";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ReportWindow rp = new ReportWindow();
            foreach(Port p in lp)
            {
                rp.textBox1.Text += Environment.NewLine;
                rp.textBox1.Text += p.name;
            }

            rp.ShowDialog();
        }
    }
}
