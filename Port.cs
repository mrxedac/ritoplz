﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

/**
 * Full credits to
 * http://www.cheynewallace.com/get-active-ports-and-associated-process-names-in-c/
 * for this ! 
 **/

/**
 * 5000 - 5500 UDP - League of Legends Game Client
 * 8393 - 8400 TCP - Patcher and Maestro
 * 2099 TCP - PVP.Net
 * 5223 TCP - PVP.Net
 * 5222 TCP - PVP.Net
 * 80 TCP - HTTP Connections
 * 443 TCP - HTTPS Connections
 * 8088 Spectator Mode */
public class Port
{
    public string name
    {
        get
        {
            return string.Format("{0} ({1} port {2})", this.process_name, this.protocol, this.port_number);
        }
        set { }
    }

    public string port_number { get; set; }
    public string process_name { get; set; }
    public string protocol { get; set; }

    public static List<Port> GetNetStatPorts()
    {
        var Ports = new List<Port>();

        try
        {
            using (Process p = new Process())
            {

                ProcessStartInfo ps = new ProcessStartInfo();
                ps.Arguments = "-a -n -o";
                ps.FileName = "netstat.exe";
                ps.UseShellExecute = false;
                ps.WindowStyle = ProcessWindowStyle.Hidden;
                ps.RedirectStandardInput = true;
                ps.RedirectStandardOutput = true;
                ps.RedirectStandardError = true;

                p.StartInfo = ps;
                p.Start();

                StreamReader stdOutput = p.StandardOutput;
                StreamReader stdError = p.StandardError;

                string content = stdOutput.ReadToEnd() + stdError.ReadToEnd();
                string exitStatus = p.ExitCode.ToString();

                if (exitStatus != "0")
                {
                    // Command Errored. Handle Here If Need Be
                }

                //Get The Rows
                string[] rows = Regex.Split(content, "\r\n");
                foreach (string row in rows)
                {
                    //Split it baby
                    string[] tokens = Regex.Split(row, "\\s+");
                    if (tokens.Length > 4 && (tokens[1].Equals("UDP") || tokens[1].Equals("TCP")))
                    {
                        string localAddress = Regex.Replace(tokens[2], @"\[(.*?)\]", "1.1.1.1");
                        string pt = localAddress.Split(':')[1];
                        int portint = int.Parse(pt);
                        if ( /* Maestro */ (tokens[1] == "TCP" && portint >= 8393 && portint <= 8400)
                            || /* LoL game client */ (tokens[1] == "UDP" && portint >= 5000 && portint <= 5500)
                            || /* PVP.Net */ (tokens[1] == "TCP" && (portint == 2099 || portint == 5222 || portint == 5223))
                            || /* HTTP(S) */ (tokens[1] == "TCP" && (portint == 80 || portint == 443))
                            || /* Spectator mode */ (portint == 8088))
                        {
                            Ports.Add(new Port
                            {
                                protocol = localAddress.Contains("1.1.1.1") ? String.Format("{0}v6 local", tokens[1]) : String.Format("{0}v4 local", tokens[1]),
                                port_number = localAddress.Split(':')[1],
                                process_name = tokens[1] == "UDP" ? LookupProcess(Convert.ToInt16(tokens[4])) : LookupProcess(Convert.ToInt16(tokens[5]))
                            });
                        }

                        /* We should also check sockets connecting to a local address on a League port */
                        string remoteAddress = Regex.Replace(tokens[3], @"\[(.*?)\]", "1.1.1.1");
                        string remotePt = remoteAddress.Split(':')[1];
                        if(!(remotePt == "*")) /* Ignore this crap */
                        {
                            int remotePtInt = int.Parse(remotePt);
                            if (remoteAddress.Contains("127.0.0.1") /* Localhost remote address */
                                && ( /* Maestro */ (tokens[1] == "TCP" && remotePtInt >= 8393 && remotePtInt <= 8400)
                                || /* LoL game client */ (tokens[1] == "UDP" && remotePtInt >= 5000 && remotePtInt <= 5500)
                                || /* PVP.Net */ (tokens[1] == "TCP" && (remotePtInt == 2099 || remotePtInt == 5222 || remotePtInt == 5223))
                                || /* HTTP(S) */ (tokens[1] == "TCP" && (remotePtInt == 80 || remotePtInt == 443))
                                || /* Spectator mode */ (remotePtInt == 8088)))
                            {
                                Ports.Add(new Port
                                {
                                    protocol = remoteAddress.Contains("1.1.1.1") ? String.Format("{0}v6 local port " + pt + " bound to", tokens[1]) : String.Format("{0}v4 local port " + pt + " bound  to", tokens[1]),
                                    port_number = remoteAddress.Split(':')[1],
                                    process_name = tokens[1] == "UDP" ? LookupProcess(Convert.ToInt16(tokens[4])) : LookupProcess(Convert.ToInt16(tokens[5]))
                                });
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            MessageBox.Show(ex.Message);
        }
        return Ports;
    }

    public static string LookupProcess(int pid)
    {
        string procName;
        try { procName = Process.GetProcessById(pid).ProcessName; }
        catch (Exception) { procName = "-"; }
        return procName;
    }
}
