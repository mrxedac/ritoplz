# README #

This tool is designed to detect which processes could lead to a conflict with League of Legends.

### HowTo: Run ###

Just open the .sln file with Visual Studio (I used Community 2015) and compile it. This should be enough.

### HowTo: Use ###

When run, the tool displays the processes currently running which could cause some problems. You can get a textual report of this by clicking the "x9 rito" button, or exit the application using the "/ff" button.